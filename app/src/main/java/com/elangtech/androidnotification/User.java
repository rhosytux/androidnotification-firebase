package com.elangtech.androidnotification;

/**
 * Created by ElangTECH on 12/1/18.
 */
public class User {

    public String email;
    public String token;

    public User(String email, String token) {
        this.email = email;
        this.token = token;
    }
}
